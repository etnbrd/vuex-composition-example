import Chance from 'chance';
import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import List from '../src/store/noteList';
import api from '../src/api';

const chance = Chance();

const localVue = createLocalVue();
localVue.use(Vuex);


let store = new Vuex.Store({
  state: {
    items: []
  },
  actions: {
    addItem({ commit }, { item }) {
      commit('addItem', { item });
    }
  },
  mutations: {
    addItem(state, { item }) {
      state.items.push(item);
    }
  }
});
store.registerModule('notes', List(store, api));


function stubItem() {
  return {
    name: chance.sentence({words: 4}),
    message: chance.paragraph({sentences: 10})
  }
}

function forArray(N) {
  return () => {

    const stubs = Array.apply(null, {length: N}).map(stubItem);

    describe('loop pushing', () => {

      test('execution time', () => {
        const start = performance.now();
        stubs.forEach((item) => store.dispatch('addItem', { item }))
        const time = performance.now() - start;

        console.log(">> ", store);

        results['push' + N] = time;
      })


    })

    describe('module registering', () => {

      test('execution time', () => {
        const start = performance.now();
        stubs.forEach((item) => store.dispatch('notes/addItem', { item }))
        const time = performance.now() - start;

        console.log(">> ", store);

        results['register' + N] = time;
      })


    })
  }
};

// Generate an array of items.
const results = {};
const Ns = [10] // , 1000];

describe('performance', () => {
  afterAll(() => {
    console.log(results);

  })

  Ns.forEach((N) => {
    describe('performance on ' + N + ' array', forArray(N));
  })
})
