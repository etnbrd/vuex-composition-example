export default function composer(module, ...behaviors) {
  return {
    // Include everything from the main module (the namespaced attribute, for example)
    ...module,


    // The state function calls, and compose the resulting state of the functions of
    // all the behaviors, and finally overwrites it with the result from the main module.
    state() {

      const init = typeof module.state === "function"
        ? module.state()
        : module.state || {};
      return Object.assign({},
        // Returns an array of instantiated state functions from the behaviors
        // Then spreads it into the arguments of Object.assign, to extend the object
        ...behaviors.map(behavior => behavior.state && behavior.state(init)),
        init
      );
    },


    // Populate the three static attributes with the behaviors attributes,
    // overwritten by the main module
    ...['getters', 'actions', 'mutations'].reduce((composed, attr) => {

      // Accumulates the defined attributes into `composed`
      return {
        ...composed,
        [attr]: Object.assign({},
          ...behaviors.map(behavior => behavior[attr]),
          module[attr]
        )
      };
    }, {})
  };
}
