const identity = x => x;

export default function proxifier(module, Proxy) {

  const proxy = Proxy(module);

  return {
    // Include everything from the main module (the namespaced attribute, for example)
    ...module,


    // The state function calls the state function from the proxy.
    // This latter function can modify and returns the original state from the module.
    state() {

      const init = typeof module.state === "function"
        ? module.state()
        : module.state || {};

      const proxify = typeof proxy.state === "function"
        ? proxy.state
        : identity;

      return proxify(init)
    },


    // Call the proxy to proxify the three static attributes
    ...['getters', 'actions', 'mutations'].reduce((composed, attr) => {

      const children = module[attr] || {};

      const proxify = typeof proxy[attr] === "function"
        ? proxy[attr]
        : identity;

      const result = (module[attr] || proxy[attr])
        ? { [attr]: proxify(children) }
        : {}

      // Accumulates the defined attributes into `composed`
      return {
        ...composed,
        ...result
      }
    }, {})
  };
}
