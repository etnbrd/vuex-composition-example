import { cloneDeep } from 'lodash';

export default function() {
  return {

    state(init) {
      return {
        __saved: cloneDeep(init)
      };
    },


    getters: {

      isModified(state) {
        // Exclude __saved from the state to compare
        const { __saved, ...draft } = state;
        // very simple comparison
        return !Object.keys(draft).every(key => __saved[key] === draft[key]);
      }
    },


    actions: {

      save({ commit }) {
        commit('save');
      },

      revert({ commit }) {
        commit('revert');
      }
    },


    mutations: {

      revert(state) {
        Object.assign(state, cloneDeep({ ...state.__saved }));
      },

      save(state) {
        // Exclude __saved from the state to save
        const { __saved, ...draft } = state;
        Object.assign(state, cloneDeep({ __saved: { ...draft } }));
      }
    }
  };
}
