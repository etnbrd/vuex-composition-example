import Vue from 'vue';

const generateId = () => Math.random().toString(36).substring(7);

export default function({ initItem }) {
  return {

    state() {
      return {
        items: {}
      }
    },


    getters: {

      item: (state) => ({ id }) => state.items[id]

    },


    actions: {

      addItem({ commit }) {
        const id = generateId();
        const child = initItem();

        commit('addItem', { id, child });

        return id;
      },

      deleteItem({ commit }, { id }) {
        commit('deleteItem', { id });
      }
    },


    mutations: {

      addItem(state, { id, child } ) {
        Vue.set(state.items, id, child);
      },

      deleteItem(state, { id }) {
        Vue.delete(state.items, id);
      }

    }
  }
}
