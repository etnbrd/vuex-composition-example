import composer from './helpers/composer';
import proxifier from './helpers/proxifier';

import ListProxy from './proxies/list';
import List from './behaviors/list'
import Note from './note';

export default composer(
  proxifier(Note, ListProxy),
  List({ initItem: Note.state })
);
