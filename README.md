# Vuex composition example

This repository contains a simple note taking application to illustrate two concept developped around the composition of vuex stores modules: mixins and proxies.
- **Mixin composition** allows to decorate a module to add some behaviors.
- **Proxy composition** allows to transform the interface that a module provide.

## Mixin composition

This application allows a user to create notes (just a name and a message).
The base vuex module for a note is implemented in [`store/note.js`](https://gitlab.com/etnbrd/vuex-composition-example/blob/master/src/store/note.js), and is extended with the draft behavior allowing to save and revert to a previous state while editing the note. This behavior is implemented in [`store/behaviors/draft.js`](https://gitlab.com/etnbrd/vuex-composition-example/blob/master/src/store/behaviors/draft.js).
And the two are composed with the `storeComposer`, implemented in [`store/helpers/composer.js`](https://gitlab.com/etnbrd/vuex-composition-example/blob/master/src/store/helpers/composer.js).

## Proxy composition

The store needs to contains a list of notes. So the `note` module is turned into a `noteList` module, by proxifying all of its state, actions, getters and setters so as to store a list of notes and specify a note id.
The proxy is implemented in [`store/proxies/list/index.js`](https://gitlab.com/etnbrd/vuex-composition-example/blob/master/src/store/proxies/list/index.js), and makes use of [`store/helpers/proxifier.js`](https://gitlab.com/etnbrd/vuex-composition-example/blob/master/src/store/helpers/proxifier.js) to proxify the `note` module.
The resulting `noteList` module is implemented in [`store/noteList.js`](https://gitlab.com/etnbrd/vuex-composition-example/blob/master/src/store/noteList.js).

---

To run the application yourself, and tinker with the code:

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```
